<?php class RandomImage {

    public $numImages = 21;
    public $imagePrefix = '/rotateimages/playworks-';
    public $imageSuffix = '.jpg';
    public $usedImages = array();

    public function get() {
        return $this->imagePrefix . $this->getRandomNum() . $this->imageSuffix;
    }

    public function getRandomNum() {
        $rand = rand(1, $this->numImages);

        if (in_array($rand, $this->usedImages)) {
            return $this->getRandomNum();
        } else {
            $this->usedImages[] = $rand;
            return $rand;
        }
    }
}