<?php
    App::import('Vendor', 'RandomImage');
    $RandomImage = new RandomImage();
?>
 <section class="singleHeader">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 gameinfo">
                <h1><?php echo $game['Game']['name']; ?></h1>
                <p>Games & Activities for kids & adults of all ages</p>
                <div class="statPanel">
                    <div class="stat">
                        <div><?php echo $game['Game']['group_size_name']; ?></div>
                        <span>kids</span>
                    </div>
                    <div class="stat">
                        <div><?php echo $game['Game']['age_group_name']; ?></div>
                        <span>grade</span>
                    </div>
                    <div class="stat">
                        <div><?php echo $game['Game']['num_highfives']; ?></div>
                        <span>fives</span>
                    </div>
                    <div class="stat">
                        <div><?php echo $game['Game']['game_length_name']; ?></div>
                        <span>min.</span>
                    </div>
                </div>
                <!--<button class="printbutton">
                    <div class="print left">Print Instructions</div>
                    <div class="right"></div>
                </button>-->
            </div>
            <div class="col-sm-6 col-md-6">
                <?php if (!empty($game['Game']['overview_video'])) { ?>
                    <iframe src="//player.vimeo.com/video/<?php echo $game['Game']['overview_video']; ?>" width="555" height="308" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                <?php } elseif (!empty($game['Game']['overview_img'])) { ?>
                    <img src="<?php echo $game['Game']['overview_img']; ?>" class="img-responsive singleGameTopImage hidden-xs" />
                <?php } else { ?>
                   <img src="<?php echo $RandomImage->get(); ?>" class="img-responsive singleGameTopImage hidden-xs" />
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="mainContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-8 col-sm-6 col-md-6 singlePreview">
                    <ul>
                        <li><b>Development Goals:</b> <span><?php echo $game['Game']['development_goals']; ?></span></li>
                        <li><b>Skills:</b> <span><?php echo $game['Game']['skills']; ?></span></li>
                    </ul>
                </div>
                <div class="col-xs-4 col-sm-6 col-md-6 highfive text-right">
                    <a href="#" class="btn-high-five" data-gameid="<?php echo $game['Game']['id']; ?>">
                        <img src="/img/singleGame/highFive.png" />
                    </a>
                </div>
                <div class="clear"></div>
                <div class="col-md-12">
                    <h2>Ready...</h2>
                </div>
                <div class="col-md-6">
                    <div class="singleReady">
                        <div class="singleTextContainer">
                            <?php echo $game['Game']['ready_html']; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="singleImage">
                        <?php if (!empty($game['Game']['ready_img'])) { ?>
                            <img src="<?php echo $game['Game']['ready_img']; ?>" class="img-responsive" />
                        <?php } else { ?>
                            <img src="<?php echo $RandomImage->get(); ?>" class="img-responsive" />
                        <?php } ?>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <div class="col-md-12 setTitle">
                    <h2>SET...</h2>
                </div>
                <div class="col-md-6">
                    <div class="singleReady singlePlay">
                        <div class="singleTextContainer">
                            <?php echo $game['Game']['set_html']; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="singleImage">
                        <?php if (!empty($game['Game']['set_img'])) { ?>
                            <img src="<?php echo $game['Game']['set_img']; ?>" class="img-responsive" />
                        <?php } else { ?>
                            <img src="<?php echo $RandomImage->get(); ?>" class="img-responsive" />
                        <?php } ?>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <div class="col-md-12 goTitle">
                    <h2>GO!</h2>
                </div>
                <div class="col-md-6">
                    <div class="singleReady singleGo">
                        <div class="singleTextContainer">
                            <?php echo $game['Game']['go_html']; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="singleImage">
                        <?php if (!empty($game['Game']['go_img'])) { ?>
                            <img src="<?php echo $game['Game']['go_img']; ?>" class="img-responsive" />
                        <?php } else { ?>
                            <img src="<?php echo $RandomImage->get(); ?>" class="img-responsive" />
                        <?php } ?>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <!--
                <div class="col-md-12">
                    <h2>Why we love this game...</h2>
                </div>
                <div class="col-md-12">
                    <div class="item blue">
                        Conflict Resolution
                    </div>
                    <div class="item lightblue">
                        Team Work
                    </div>
                    <div class="item green">
                        Communication
                    </div>
                    <div class="item orange">
                        Empathy
                    </div>
                    <div class="item purple">
                        Leadership
                    </div>
                </div>-->
            </div>
        </div>
    </div>
    <div class="gameCategories singleGameAllCategories">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Game Categories</h2>
                </div>
            </div>
            <div class="row">
                <?php foreach($categories as $category): ?>
                    <div class="col-md-3">
                        <div class="gameCircle">
                            <img src="<?php echo $category['Category']['icon_img']; ?>" />
                        </div>
                        <div class="gameCategory">
                            <h3><?php echo $category['Category']['name']; ?></h3>
                            <p><?php echo $this->Text->truncate($category['Category']['short_description'],60,array('ellipsis'=>'...', 'exact'=>false)); ?></p>
                            <a href="/games/category/<?php echo $category['Category']['slug']; ?>">Go</a>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>






<?php
// echo $game['AgeGroup']['name'];
// echo $game['Game']['development_goals'];
// debug($game);
?>