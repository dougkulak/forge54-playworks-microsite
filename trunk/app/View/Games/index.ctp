<section>
    <div class="gameCategories">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Searchable Database of games and activities for kids & adults of all ages.</h2>
                </div>
            </div>
            <div class="row">
                <?php foreach($games as $game): ?>
                    <div class="col-md-3">
                        <div class="gameCircle">
                            <img src="img/test/iconTest.jpg" />
                        </div>
                        <div class="gameCategory">
                            <h3><?php echo $game['Game']['name']; ?></h3>
                            <p>Tools & strategies to create fun and safe place for all children.</p>
                            <a href="#">View Game</a>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>


