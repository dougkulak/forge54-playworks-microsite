<div class="games form">
<?php echo $this->Form->create('Game',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Game'); ?></legend>
	<?php
		echo $this->Form->input('id');
        echo $this->Form->input('game_length_id');
        echo $this->Form->input('game_length_name');
        echo $this->Form->input('level_id');
        echo $this->Form->input('GroupSize');
        echo $this->Form->input('group_size_name');
        echo $this->Form->input('AgeGroup');
        echo $this->Form->input('age_group_name');
        echo $this->Form->input('Category');
		echo $this->Form->input('name');
		echo $this->Form->input('development_goals');
        echo $this->Form->input('skills');
		echo $this->Form->input('num_highfives');
        echo $this->Form->input('overview_img', array('type' => 'file'));
        if (!empty($this->data['Game']['overview_img'])) {
            echo 'Existing File: ' . $this->data['Game']['overview_img'] . '<br>';
            echo $this->Html->image($this->data['Game']['overview_img']);
        }
		echo $this->Form->input('overview_video');
		echo $this->Form->input('ready_text');
		echo $this->Form->input('ready_img', array('type' => 'file'));
        if (!empty($this->data['Game']['ready_img'])) {
            echo 'Existing File: ' . $this->data['Game']['ready_img'] . '<br>';
            echo $this->Html->image($this->data['Game']['ready_img']);
        }
		echo $this->Form->input('set_text');
		echo $this->Form->input('set_img', array('type' => 'file'));
        if (!empty($this->data['Game']['set_img'])) {
            echo 'Existing File: ' . $this->data['Game']['set_img'] . '<br>';
            echo $this->Html->image($this->data['Game']['set_img']);
        }
        echo $this->Form->input('go_text');
		echo $this->Form->input('go_img', array('type' => 'file'));
        if (!empty($this->data['Game']['go_img'])) {
            echo 'Existing File: ' . $this->data['Game']['go_img'] . '<br>';
            echo $this->Html->image($this->data['Game']['go_img']);
        }
		echo $this->Form->input('equipment');
		echo $this->Form->input('inclusive');
		echo $this->Form->input('transitional');
		echo $this->Form->input('good_for_the_classroom');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Game.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Game.id'))); ?></li>
	</ul>
</div>
