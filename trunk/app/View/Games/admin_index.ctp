<div class="games index">
	<h2><?php echo __('Games'); ?></h2>
    <ul class="actions">
        <li><?php echo $this->Html->link(__('New Game'), array('action' => 'add'), array('class' => array('btn btn-default'))); ?></li>
    </ul>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
        <th><?php echo $this->Paginator->sort('id'); ?></th>
        <th><?php echo $this->Paginator->sort('name'); ?></th>
        <th><?php echo $this->Paginator->sort('game_length_id'); ?></th>
        <th><?php echo $this->Paginator->sort('level_id'); ?></th>
        <th><?php echo $this->Paginator->sort('num_highfives'); ?></th>
        <th><?php echo $this->Paginator->sort('created'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($games as $game): ?>
	<tr>
		<td><?php echo h($game['Game']['id']); ?>&nbsp;</td>
        <td><?php echo h($game['Game']['name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($game['GameLength']['name'], array('controller' => 'game_lengths', 'action' => 'view', $game['GameLength']['id'])); ?>
		</td>
        <td>
            <?php echo $this->Html->link($game['Level']['name'], array('controller' => 'levels', 'action' => 'view', $game['Level']['id'])); ?>
        </td>
		<td><?php echo h($game['Game']['num_highfives']); ?>&nbsp;</td>
		<td><?php echo h($game['Game']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $game['Game']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $game['Game']['id']), array(), __('Are you sure you want to delete # %s?', $game['Game']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>