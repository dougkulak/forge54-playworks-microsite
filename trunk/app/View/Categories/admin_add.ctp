<div class="categories form">
<?php echo $this->Form->create('Category',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Category'); ?></legend>
	<?php
		echo $this->Form->input('name');
        echo $this->Form->input('slug');
        echo $this->Form->input('description');
        echo $this->Form->input('short_description');
        echo $this->Form->input('icon_img', array('type' => 'file'));
        if (!empty($this->data['Category']['icon_img'])) {
            echo 'Existing File: ' . $this->data['Category']['icon_img'] . '<br>';
            echo $this->Html->image($this->data['Category']['icon_img']);
        }
		echo $this->Form->input('Game');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
