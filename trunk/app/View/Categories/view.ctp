<?php
    App::import('Vendor', 'RandomImage');
    $RandomImage = new RandomImage();
?>
<section>
    <div class="singleCategory">
        <div class="container">
            <div class="row">
                <div class="col-md-6 singleCategoryTitle">
                    <div class="gameCircle"><img src="http://www.microsite.playworks.rocks/<?php echo $category['Category']['icon_img']; ?>" /></div>
                    <h1><?php echo $category['Category']['name']; ?></h1>
                    <div class="clear"></div>
                    <p><?php echo $category['Category']['description']; ?></p>
                    <button class="btn-toggle-filter">
                        <div class="left">Refine Search</div>
                        <div class="right"><i class="fa fa-arrow-circle-o-down"></i></div>
                    </button>
                </div>
                <div class="col-md-6 singleCategoryImage">
                    <img src="<?php echo $RandomImage->get(); ?>" class="img-responsive" />
                </div>
            </div>
        </div>

        <div class="hideFilters" id="filters" style="display: none">
            <form method="post">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Find Your Game!</h2>
                        </div>
                        <div class="col-md-3">
                            <?php echo $this->Form->input('keyword', array('class' => 'form-control', 'label' => false, 'placeholder' => 'Enter Keywords')); ?>
                            <?php echo $this->Form->input('categories', array('class' => 'form-control', 'label' => false, 'empty' => 'Select Category')); ?>
                            <?php echo $this->Form->input('groupSizes', array('class' => 'form-control', 'label' => false, 'empty' => 'Select Group Size')); ?>
                            <?php echo $this->Form->input('ageGroups', array('class' => 'form-control', 'label' => false, 'empty' => 'Select Age Group')); ?>
                            <?php echo $this->Form->input('levels', array('class' => 'form-control', 'label' => false, 'empty' => 'Select Level Difficulty')); ?>
                            <?php echo $this->Form->input('gameLengths', array('class' => 'form-control', 'label' => false, 'empty' => 'Select Game Length')); ?>
                        </div>
                        <div class="col-md-3">
                            <div class="radioLabel">
                                <label name="inclusive">
                                    <div class="right">
                                        <div class="option">
                                            No<br />
                                            <input type="radio" name="data[inclusive]" value="0"/>
                                        </div>
                                        <div class="option">
                                            Yes<br />
                                            <input type="radio" name="data[inclusive]" value="1"/>
                                        </div>
                                    </div>
                                    <div class="left">
                                        Inclusive
                                    </div>
                                </label>
                            </div>
                            <div class="radioLabel">
                                <label name="transitional">
                                    <div class="right">
                                        <div class="option">
                                            No<br />
                                            <input type="radio" name="data[transitional]" value="0" />
                                        </div>
                                        <div class="option">
                                            Yes<br />
                                            <input type="radio" name="data[transitional]" value="1" />
                                        </div>
                                    </div>
                                    <div class="left">
                                        Transitional
                                    </div>
                                </label>
                            </div>
                            <div class="radioLabel">
                                <label name="equipment">
                                    <div class="right">
                                        <div class="option">
                                            No<br />
                                            <input type="radio" name="data[equipment]" value="0"/>
                                        </div>
                                        <div class="option">
                                            Yes<br />
                                            <input type="radio" name="data[equipment]" value="1"/>
                                        </div>
                                    </div>
                                    <div class="left">
                                        Equipment?
                                    </div>
                                </label>
                            </div>
                            <div class="radioLabel">
                                <label name="classroom">
                                    <div class="right">
                                        <div class="option">
                                            No<br />
                                            <input type="radio" name="data[classroom]" value="0"/>
                                        </div>
                                        <div class="option">
                                            Yes<br />
                                            <input type="radio" name="data[classroom]" value="1" />
                                        </div>
                                    </div>
                                    <div class="left">
                                        Good for Classroom?
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-offset-2 col-md-2">
                            <button class="btn-search">
                                <div class="left"><i class="fa fa-search"></i> &nbsp; Search</div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="container search-results-loader" style="display: none">
            <div class="row text-center">
                <h3>Loading</h3>
                <p><i class="fa fa-spinner fa-spin"></i></p>
            </div>
        </div>
        <div class="container search-results-container" style="display: none">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>Search Results</h3>
                </div>
                <div id="searchResultTemplate" class="col-md-4 hide topRated">
                    <a href="/games/view/{{id}}" class="link">
                        <img src="/img/singleCategory/playButton.png" class="playButton" />
                        <img src="/img/singleCategory/thumbGame.jpg" class="img-responsive thumbGame" />
                        <div class="topRatedTitle name">
                            {{name}}
                        </div>
                        <div class="topRatedStats stats">
                            <div class="stat">
                                <div class="groupSize">{{groupSize}}</div>
                                <span>kids</span>
                            </div>
                            <div class="stat">
                                <div class="ageGroup">{{ageGroup}}</div>
                                <span>grade</span>
                            </div>
                            <div class="stat">
                                <div class="highFives">{{numHighFives}}</div>
                                <span>fives</span>
                            </div>
                            <div class="stat">
                                <div class="gameLength">{{gameLength}}</div>
                                <span>min.</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div id="searchResults"></div>
            </div>
        </div>
        <div class="container top-rated-container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3>Top Rated in Category</h3>
                </div>
            </div>
            <div class="row">
                <?php foreach ($topGames as $game): ?>
                    <div class="col-md-4 topRated">
                        <a href="/games/view/<?php echo $game['Game']['id']; ?>">
                            <img src="/img/singleCategory/playButton.png" class="playButton" />
                            <img src="/img/singleCategory/thumbGame.jpg" class="img-responsive thumbGame" />
                            <div class="topRatedTitle">
                                <?php echo $game['Game']['name']; ?>
                            </div>
                            <div class="topRatedStats">
                                <div class="stat">
                                    <div><?php echo $game['Game']['group_size_name']; ?></div>
                                    <span>kids</span>
                                </div>
                                <div class="stat">
                                    <div><?php echo $game['Game']['age_group_name']; ?></div>
                                    <span>grade</span>
                                </div>
                                <div class="stat">
                                    <div><?php echo $game['Game']['num_highfives']; ?></div>
                                    <span>fives</span>
                                </div>
                                <div class="stat">
                                    <div><?php echo $game['Game']['game_length_name']; ?></div>
                                    <span>min.</span>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</section>



<?php
// debug($category);
//debug($topGames);
?>