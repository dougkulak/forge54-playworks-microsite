<div class="categories form">
<?php echo $this->Form->create('Category',array('enctype'=>'multipart/form-data')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Category'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('slug');
		echo $this->Form->input('description');
		echo $this->Form->input('short_description');
        echo $this->Form->input('icon_img', array('type' => 'file'));
        if (!empty($this->data['Category']['icon_img'])) {
            echo 'Existing File: ' . $this->data['Category']['icon_img'] . '<br>';
            echo $this->Html->image($this->data['Category']['icon_img']);
        }
		echo $this->Form->input('Game');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Category.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?></li>
	</ul>
</div>
