<section>
    <div class="gameCategories">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>​Pick a category of the games you want to play.</h2>
                </div>
            </div>
            <div class="row">
                <?php foreach($categories as $category): ?>
                <a href="/games/category/<?php echo $category['Category']['slug']; ?>"><div class="col-md-3">
                        <div class="gameCircle">
                            <img src="http://www.microsite.playworks.rocks/<?php echo $category['Category']['icon_img']; ?>" />
                        </div>
                        <div class="gameCategory">
                            <h3><?php echo $category['Category']['name']; ?></h3>
                            <p><?php echo $this->Text->truncate($category['Category']['short_description'],60,array('ellipsis'=>'...', 'exact'=>false)); ?></p>
                            <a href="/games/category/<?php echo $category['Category']['slug']; ?>">Go</a>
                        </div>
                    </div></a>
                <?php endforeach; ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>