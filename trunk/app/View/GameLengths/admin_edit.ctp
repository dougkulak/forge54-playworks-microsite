<div class="gameLengths form">
<?php echo $this->Form->create('GameLength'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Game Length'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('GameLength.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('GameLength.id'))); ?></li>
	</ul>
</div>
