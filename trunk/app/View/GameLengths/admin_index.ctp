<div class="gameLengths index">
	<h2><?php echo __('Game Lengths'); ?></h2>
    <ul class="actions">
    		<li><?php echo $this->Html->link(__('New Game Length'), array('action' => 'add'), array('class' => 'btn btn-default')); ?></li>
    	</ul>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($gameLengths as $gameLength): ?>
	<tr>
		<td><?php echo h($gameLength['GameLength']['id']); ?>&nbsp;</td>
		<td><?php echo h($gameLength['GameLength']['name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $gameLength['GameLength']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $gameLength['GameLength']['id']), array(), __('Are you sure you want to delete # %s?', $gameLength['GameLength']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
