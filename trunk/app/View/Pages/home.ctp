<div class="middle">
    <div class="container">
        <div class="row">
            <div class="slogan col-xs-12 col-md-5">
                <h1 class="first">Belong, have fun &</h1>
                <h1 class="second">be part of the game.</h1>
                <a href="/download"><div class="downloadbutton"><i class="fa fa-cloud-download"></i>DOWNLOAD</div></a>
                <a href="/tour"><div class="tourbutton">Take the Tour!</div></a>
            </div>
        </div>
    </div>
</div>
<div class="homeFixed">
    <a href="/games" class="game">
        <div class="feature">
            <div class="face cover">PLAYBOOK! PLAYBOOK!</div>
            <div class="face hover">Play Games!</div>
        </div>
    </a>
    <a href="/learn" class="learn">
        <div class="feature">
            <div class="face cover">Who's Playworks?</div>
            <div class="face hover">Learn about the Playbook!</div>
        </div>
    </a>
    <a href="/tour" class="tour">
        <div class="feature">
            <div class="face cover">Check out PLAYBOOK</div>
            <div class="face hover">Tour the Playbook!</div>
        </div>
    </a>
    <a href="/download" class="download">
        <div class="feature">
            <div class="face cover">Playworks in your pocket!</div>
            <div class="face hover">Download the app!</div>
        </div>
    </a>
</div>
