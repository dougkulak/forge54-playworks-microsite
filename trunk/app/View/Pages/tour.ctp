<section class="tourHeader purple-bg">
    <div class="container">
        <div class="row">
            <div class="hidden-xs col-sm-5 col-md-5">
                <img src="/img/tour/topHeaderHand.png" alt="Top Header Image" class="img-responsive" />
            </div>
            <div class=" col-xs-12 col-sm-7 col-md-7 topHeader">
               <h1>Pocket Full of Playworks.<br />Who's in? Everybody!</h1>
                <a href="#" class="buttonDownload orange"><i class="fa fa-cloud-download"></i> Download</a>
                <a href="#" class="buttonDownload">Take the Tour</a>
                <div class="clear"></div>
                <br /><br />
                    <iframe src="//player.vimeo.com/video/106129833?title=0&amp;byline=0&amp;portrait=0" width="400" height="231" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
<section class="hidden-xs tourImageBreak">

</section>
<section class="choosePlay purple-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Welcome to PLAYBOOK 500+ games that teach<br />safe and inclusive play</h2>
            </div>
            <div class="col-md-6">
                <img src="/img/tour/chooseIphone.png" class="img-responsive" />
            </div>
            <div class="col-md-6 chooseCategories">
                <h4>Game Categories</h4>
                <ul>
                    <li>
                        <img src="img/tour/icon-1.png" />
                        <p>Ice Breakers</p>
                    </li>
                    <li>
                        <img src="img/tour/icon-2.png" />
                        <p>Cooperating Games</p>
                    </li>
                    <li>
                        <img src="img/tour/icon-3.png" />
                        <p>Readiness Games</p>
                    </li>
                    <li>
                        <img src="img/tour/icon-4.png" />
                        <p>Minute Moves</p>
                    </li>
                    <li>
                        <img src="img/tour/icon-5.png" />
                        <p>Tag Games</p>
                    </li>
                    <li>
                        <img src="img/tour/icon-6.png" />
                        <p>Health & Fitness</p>
                    </li>
                    <li>
                        <img src="img/tour/icon-7.png" />
                        <p>Core Playground</p>
                    </li>
                    <li>
                        <img src="img/tour/icon-8.png" />
                        <p>Suggested Modifications</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="tourImageBreak2 hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-1">
                <img src="/img/tour/orangeBall.png" alt="Orange Ball" class="img-responsive orangeBall hidden-xs" />
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<section class="finalChoose purple-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3 col-md-offset-7">
                    <img src="/img/tour/bottomCircle.png" alt="Bottom Circle" class="img-responsive bottomCircle hidden-xs" />
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="bottomCounselor">
                <div class="col-md-offset-4 col-md-8">
                    <h3>Download The App!</h3>
                    <div class="appDownload">
                        <a href="#">
                           <img src="/img/tour/appDownload.png" class="appDownloadButton" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>