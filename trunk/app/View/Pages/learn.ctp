<section class="learnHeader">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 who text-center">
                <h1>Who is Playworks?</h1>
                <p>We believe in the power of play to make kids, schools, and communities.</p>
            </div>
        </div>
    </div>
    <div class="learnCounselors">
        <div class="container">
            <div class="row">
                <div class="why col-md-5 col-xs-12 ">
                    <h2>Why Playworks?</h2>
                    <p>Playworks games teach kids how to play nice and safe. Through leadership and empathy, they grow confident, respectful, and generous.</p>
                </div>
                <div class="people col-md-7 col-xs-12 ">
                    <img src="/img/learn/people.png" class="img-responsive" />
                </div>
            </div>
            <div class="kidsgroup col-sm-6 col-md-6 col-xs-12 ">
                <img src="/img/learn/kidsgroup.png" class="img-responsive" />
            </div>
            <div class="col-sm-6 col-md-6">
                <h2>What is the IMPACT?</h2>
                <p>Playworks cuts down bullying, and ups the fun, action and learning. At schools, at home. It gives kids a healthy release at recess and teaches them to resolve their own conflicts in a healthy way. So they come back to class refreshed and more ready to learn. Playworks lets admins do their jobs instead of disciplining kids. Scores go up, and attendance, too. So schools get more play in more ways than one.</p>
            </div>
        </div>
    </div>
</section>
<section class="learnBreak2">
</section>
<section class="learnFinal">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 ">
                <h2>What is a Playworks Playground like?</h2>
                <ul>
                    <li>Everyone’s in the game contributing</li>
                    <li>Cheers are loud, kids are proud</li>
                    <li>High fives, even tens sometimes</li>
                    <li>Kids become leaders</li>
                    <li>Spats settled on the go - with Ro Sham Bo</li>
                </ul>
            </div>
            <div class="col-md-6 col-xs-12 ">
                <img src="/img/learn/learnImage.jpg" class="img-responsive" />
            </div>
            <div class="col-md-12 learnFinalTagline text-center">
                <h3>How can I participate in Playworks?</h3>
                <p>Visit <a href="http://www.playworks.org">www.playworks.org</a> to learn more about the power of play</p>
<!--                <ul class="social">-->
<!--                    <li><img src="/img/learn/twitter.png" class="img-responsive"/></li>-->
<!--                    <li><img src="/img/learn/pinterest.png" class="img-responsive"/></li>-->
<!--                    <li><img src="/img/learn/facebook.png" class="img-responsive"/></li>-->
<!--                    <li><img src="/img/learn/vimeo.png" class="img-responsive"/></li>-->
<!--                </ul>-->
            </div>
        </div>
    </div>
</section>