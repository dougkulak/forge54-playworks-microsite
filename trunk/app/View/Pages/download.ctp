<section class="downloadBG">
    <div class="container downloadHeader">
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <h1>Welcome to The Playbook by Playworks, with over 500 games to get kids moving on any playground.</h1>
            </div>
            <div class="col-xs-12 col-md-3">
                <a href="#">
                    <img src="/img/download/appIcon.png" class="img-responsive" />
                </a>
            </div>
        </div>
    </div>
    <div class="downloadBreak">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Playworks games and techniques improve the health and well-being of children by increasing opportunities for safe, meaningful play and physical activity.</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="downloadWatch">
                    <div class="col-md-6 col-md-offset-5">
                        <h3>Watch Playworks in action!</h3>
                        <p>Healthy play teaches kids how to get along and gets their hearts pumping. It encourages every boy, girl, and adult to jump into the game.Playworks teaches kids to be respectful of one another at school and at home. Our games build positive relationships through play and friendly competition.</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="videoWrapper">
                    <iframe src="//player.vimeo.com/video/106129833?byline=0&amp;portrait=0" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-4 downloadCTA">
                <a href="#">
                    <img src="/img/download/appIcon.png" />
                </a>
                <p>Install, Explore, Learn and Play!!</p>
                <button>
                    <div class="left"><i class="fa fa-arrow-circle-o-down"></i> Download Now!</div>
                </button>
            </div>
        </div>
    </div>
</section>