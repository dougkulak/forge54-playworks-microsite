<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <?php echo $this->Html->charset(); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>		<?php echo $title_for_layout; ?>
    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
    //echo $this->Html->css('cake.generic');
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>

    <script src="//use.typekit.net/dpc7gly.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <?php echo $this->Html->css('bootstrap.min'); ?>
    <?php echo $this->Html->css('bootstrap-theme'); ?>
    <?php echo $this->Html->css('main'); ?>
    <?php echo $this->Html->css('sub'); ?>
    <?php echo $this->Html->css('queries-300'); ?>
    <?php echo $this->Html->css('queries-400'); ?>
    <?php echo $this->Html->css('queries-768'); ?>
    <?php echo $this->Html->css('queries-992'); ?>
    <?php echo $this->Html->css('queries-1200'); ?>
    <link rel="stylesheet/less" type="text/css" href="/css/main.less">
    <link rel="stylesheet/less" type="text/css" href="/css/sub.less">
    <link rel="stylesheet/less" type="text/css" href="/css/queries-300.less">
    <link rel="stylesheet/less" type="text/css" href="/css/queries-480.less">
    <link rel="stylesheet/less" type="text/css" href="/css/queries-768.less">
    <link rel="stylesheet/less" type="text/css" href="/css/queries-992.less">
    <link rel="stylesheet/less" type="text/css" href="/css/queries-1200.less">

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/1.7.0/less.min.js"></script>
    <script src="/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <script src="/js/custom.js"></script>

</head>
<body class="<?php echo $isHome ? 'home' : ''; ?> <?php echo 'controller-' . $controller; ?> <?php echo 'action-' . $action; ?>">
<!--[if lt IE 7]>
<![endif]-->
<div class="topcolorbar"></div>

<div class="navbar navbar-inverse" role="navigation">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">
            <img src="/img/assets/mainLogo.png" alt="Playworks Logo" class="logo" />
        </a>
    </div>
    <div class="container">
        <div id="navigation" class="navbar-collapse collapse">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/games">Games</a></li>
                <li><a href="/learn">Learn</a></li>
                <li><a href="/tour">Tour</a></li>
                <li class="cta"><a href="/download"><i class="fa fa-cloud-download"> Download</i></a></li>
            </ul>
        </div>
    </div>
</div>
</div>

<?php echo $this->Session->flash(); ?>


<?php echo $this->fetch('content'); ?>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>

<script src="/js/vendor/bootstrap.min.js"></script>

<script src="/js/main.js"></script>

</body>
</html>