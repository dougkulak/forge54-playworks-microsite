<?php
App::uses('AppController', 'Controller');
/**
 * GameLengths Controller
 *
 * @property GameLength $GameLength
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class GameLengthsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->GameLength->recursive = 0;
		$this->set('gameLengths', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->GameLength->exists($id)) {
			throw new NotFoundException(__('Invalid game length'));
		}
		$options = array('conditions' => array('GameLength.' . $this->GameLength->primaryKey => $id));
		$this->set('gameLength', $this->GameLength->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->GameLength->create();
			if ($this->GameLength->save($this->request->data)) {
				$this->Session->setFlash(__('The game length has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The game length could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->GameLength->exists($id)) {
			throw new NotFoundException(__('Invalid game length'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->GameLength->save($this->request->data)) {
				$this->Session->setFlash(__('The game length has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The game length could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('GameLength.' . $this->GameLength->primaryKey => $id));
			$this->request->data = $this->GameLength->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->GameLength->id = $id;
		if (!$this->GameLength->exists()) {
			throw new NotFoundException(__('Invalid game length'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->GameLength->delete()) {
			$this->Session->setFlash(__('The game length has been deleted.'));
		} else {
			$this->Session->setFlash(__('The game length could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
