<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

    public $uses = array('Category', 'Game', 'Level', 'AgeGroup', 'GameLength', 'GroupSize');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Category->recursive = 0;
        $categories = $this->Paginator->paginate();
		$this->set('categories', $categories);
	}


    public function index() {
        $categories = $this->Category->find('all');
        $this->set('categories', $categories);
    }

    public function view($id = null)
    {
        $this->Category->contain();

        if (!is_numeric($id)) {
            $category = $this->Category->findBySlug($id);
        } else {
            $category = $this->Category->findById($id);
        }

        $this->Game->bindModel(array('hasOne' => array('CategoriesGame')));
        $this->Game->contain(array('CategoriesGame'));
        $topGames = $this->Game->find('all', array(
            'conditions' => array('CategoriesGame.category_id'=> $category['Category']['id']),
            'limit' => 6
        ));

        $levels = $this->Level->find('list');
        $ageGroups = $this->AgeGroup->find('list');
        $categories = $this->Category->find('list');
        $gameLengths = $this->GameLength->find('list');
        $groupSizes = $this->GroupSize->find('list');

        $this->set('category', $category);
        $this->set('topGames', $topGames);
        $this->set('levels', $levels);
        $this->set('ageGroups', $ageGroups);
        $this->set('categories', $categories);
        $this->set('gameLengths', $gameLengths);
        $this->set('groupSizes', $groupSizes);
    }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
		$this->set('category', $this->Category->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Category->create();

            $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
            $uploadPath = WWW_ROOT . 'img/uploads/';
            $uploadDbPath = '/img/uploads/';

            if(!empty($this->data['Category']['icon_img']['name'])) {
                $file = $this->data['Category']['icon_img'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                if(in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'], $uploadPath . $file['name']);
                    $this->request->data['Category']['icon_img'] = $uploadDbPath . $file['name'];
                }
            } else {
                unset($this->request->data['Category']['icon_img']);
            }

			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'));
			}
		}
		$games = $this->Category->Game->find('list');
		$this->set(compact('games'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
            $uploadPath = WWW_ROOT . 'img/uploads/';
            $uploadDbPath = '/img/uploads/';

            if(!empty($this->data['Category']['icon_img']['name'])) {
                $file = $this->data['Category']['icon_img'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                if(in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'], $uploadPath . $file['name']);
                    $this->request->data['Category']['icon_img'] = $uploadDbPath . $file['name'];
                }
            } else {
                unset($this->request->data['Category']['icon_img']);
            }

			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
			$this->request->data = $this->Category->find('first', $options);
		}
		$games = $this->Category->Game->find('list');
		$this->set(compact('games'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Category->delete()) {
			$this->Session->setFlash(__('The category has been deleted.'));
		} else {
			$this->Session->setFlash(__('The category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
