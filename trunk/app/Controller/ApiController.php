<?php
App::uses('AppController', 'Controller');

class ApiController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session', 'RequestHandler');

    public $uses = array('Game', 'Level', 'AgeGroup', 'Category', 'GameLength', 'GroupSize');

    public function searchGames() {
        $this->Game->bindModel(array('hasOne' => array('CategoriesGame', 'GamesGroupSize', 'AgeGroupsGame')));
        $this->Game->contain(array('GameLength', 'CategoriesGame', 'GamesGroupSize', 'AgeGroupsGame'));

        $conditions = array();

        if (isset($this->data['categories']) && !empty($this->data['categories'])) $conditions = array_merge($conditions, array('CategoriesGame.category_id' => $this->data['categories']));
        if (isset($this->data['groupSizes']) && !empty($this->data['groupSizes'])) $conditions = array_merge($conditions, array('GamesGroupSize.group_size_id' => $this->data['groupSizes']));
        if (isset($this->data['ageGroups']) && !empty($this->data['ageGroups'])) $conditions = array_merge($conditions, array('AgeGroupsGame.age_group_id' => $this->data['ageGroups']));
        if (isset($this->data['gameLengths']) && !empty($this->data['gameLengths'])) $conditions = array_merge($conditions, array('Game.game_length_id' => $this->data['gameLengths']));
        if (isset($this->data['levels']) && !empty($this->data['levels'])) $conditions = array_merge($conditions, array('Game.level_id' => $this->data['levels']));
        if (isset($this->data['equipment']) && !empty($this->data['equipment'])) $conditions = array_merge($conditions, array('Game.equipment' => $this->data['equipment']));
        if (isset($this->data['inclusive']) && !empty($this->data['inclusive'])) $conditions = array_merge($conditions, array('Game.inclusive' => $this->data['inclusive']));
        if (isset($this->data['transitional']) && !empty($this->data['transitional'])) $conditions = array_merge($conditions, array('Game.transitional' => $this->data['transitional']));
        if (isset($this->data['classroom']) && !empty($this->data['classroom'])) $conditions = array_merge($conditions, array('Game.good_for_the_classroom' => $this->data['classroom']));

        $games = $this->Game->find('all', array(
            'conditions' => $conditions
        ));

        $addedGameIds = array();
        $results = array();
        foreach ($games as $game) {
            if (!in_array($game['Game']['id'], $addedGameIds)) {
                if (isset($this->data['keyword']) && !empty($this->data['keyword'])) {
                    if (stripos($game['Game']['name'], $this->data['keyword']) !== false) {
                        $results[] = $game;
                        $addedGameIds[] = $game['Game']['id'];
                    }
                } else {
                    $results[] = $game;
                    $addedGameIds[] = $game['Game']['id'];
                }
            }
        }

        $this->layout = null;
        header('Content-Type: application/json');
        echo json_encode($results);
        exit;
    }

    public function highFive($id = null) {
        $response = array();

        if (!$id) {
            $response['error'] = 'Game ID is required.';
        } else {
            $this->Game->contain();
            $game = $this->Game->findById($id);
            if (empty($game) || $game == null) {
                $response['error'] = 'Unable to find game with ID ' . $id;
            } else {
                $game['Game']['num_highfives'] = $game['Game']['num_highfives'] + 1;
                if (!$this->Game->save($game)) {
                    $response['error'] = 'Unable to save game.';
                } else {
                    $response['success'] = true;
                }
            }
        }
        $this->layout = null;
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function getAll($debug = null) {
        $this->Category->contain();
        $this->Level->contain();
        $this->AgeGroup->contain();
        $this->GameLength->contain();
        $this->GroupSize->contain();

        $this->Game->contain(array('AgeGroup', 'Category', 'GroupSize'));

        $response = array();
        $response['games'] = $this->Game->find('all');
        $response['categories'] = $this->Category->find('all');
        $response['levels'] = $this->Level->find('all');
        $response['age_groups'] = $this->AgeGroup->find('all');
        $response['game_lengths'] = $this->GameLength->find('all');
        $response['group_sizes'] = $this->GroupSize->find('all');

        foreach ($response['games'] as &$game) {
            if (isset($game['AgeGroup']) && !empty($game['AgeGroup'])) {
                $game['Game']['age_group_ids'] = Hash::extract($game['AgeGroup'], '{n}.id');
                unset($game['AgeGroup']);
            }
            if (isset($game['GroupSize']) && !empty($game['GroupSize'])) {
                $game['Game']['group_size_ids'] = Hash::extract($game['GroupSize'], '{n}.id');
                unset($game['GroupSize']);
            }
            if (isset($game['Category']) && !empty($game['Category'])) {
                $game['Game']['category_ids'] = Hash::extract($game['Category'], '{n}.id');
                unset($game['Category']);
            }
            $game = $game['Game'];
            unset($game['Game']);
        }

        if ($debug) {
            debug($response);
            exit;
        } else {
            $this->layout = null;
            header('Content-Type: application/json');
            echo json_encode(array('response' => $response));
            exit;
        }
    }

    public function getAllCategories($debug = null) {
        $this->Category->contain();

        $response = $this->Category->find('all');

        if ($debug) {
            debug($response);
            exit;
        } else {
            $this->layout = null;
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }
    }

    public function getAllLevels($debug = null) {
        $this->Level->contain();

        $response = $this->Level->find('all');

        if ($debug) {
            debug($response);
            exit;
        } else {
            $this->layout = null;
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }
    }

    public function getAllAgeGroups($debug = null) {
        $this->AgeGroup->contain();

        $response = $this->AgeGroup->find('all');

        if ($debug) {
            debug($response);
            exit;
        } else {
            $this->layout = null;
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }
    }

    public function getAllGameLengths($debug = null) {
        $this->GameLength->contain();

        $response = $this->GameLength->find('all');

        if ($debug) {
            debug($response);
            exit;
        } else {
            $this->layout = null;
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }
    }

    public function getAllGroupSizes($debug = null) {
        $this->GroupSize->contain();

        $response = $this->GroupSize->find('all');

        if ($debug) {
            debug($response);
            exit;
        } else {
            $this->layout = null;
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }
    }

}
