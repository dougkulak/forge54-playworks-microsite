<?php
App::uses('AppController', 'Controller');
/**
 * Games Controller
 *
 * @property Game $Game
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class GamesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

    public $uses = array('Game', 'Category');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Game->recursive = 0;
		$this->set('games', $this->Paginator->paginate());
	}

    public function index() {
        $games = $this->Game->find('all');
        $this->set('games', $games);
    }



    public function view($id = null)
    {
        require_once('../Vendor/Michelf/Markdown.inc.php');

        $markdown = new Michelf\Markdown;

        $game = $this->Game->findById($id);

        $game['Game']['ready_html'] = $markdown::defaultTransform($game['Game']['ready_text']);
        $game['Game']['set_html'] = $markdown::defaultTransform($game['Game']['set_text']);
        $game['Game']['go_html'] = $markdown::defaultTransform($game['Game']['go_text']);

        $this->set('game', $game);

        $categories = $this->Category->find('all');
        $this->set('categories', $categories);
    }

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Game->exists($id)) {
			throw new NotFoundException(__('Invalid game'));
		}
		$options = array('conditions' => array('Game.' . $this->Game->primaryKey => $id));
		$this->set('game', $this->Game->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
        if ($this->request->is('post')) {
            $this->Game->create();

            $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
            $uploadPath = WWW_ROOT . 'img/uploads/';
            $uploadDbPath = '/img/uploads/';

            if(!empty($this->data['Game']['overview_img']['name'])) {
                $file = $this->data['Game']['overview_img'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                if(in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'], $uploadPath . $file['name']);
                    $this->request->data['Game']['overview_img'] = $uploadDbPath . $file['name'];
                }
            } else {
                unset($this->request->data['Game']['overview_img']);
            }

            if(!empty($this->data['Game']['ready_img']['name'])) {
                $file = $this->data['Game']['ready_img'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                if(in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'], $uploadPath . $file['name']);
                    $this->request->data['Game']['ready_img'] = $uploadDbPath . $file['name'];
                }
            } else {
                unset($this->request->data['Game']['ready_img']);
            }

            if(!empty($this->data['Game']['set_img']['name'])) {
                $file = $this->data['Game']['set_img'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                if(in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'], $uploadPath . $file['name']);
                    $this->request->data['Game']['set_img'] = $uploadDbPath . $file['name'];
                }
            } else {
                unset($this->request->data['Game']['set_img']);
            }

            if(!empty($this->data['Game']['go_img']['name'])) {
                $file = $this->data['Game']['go_img'];
                $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                if(in_array($ext, $arr_ext)) {
                    move_uploaded_file($file['tmp_name'], $uploadPath . $file['name']);
                    $this->request->data['Game']['go_img'] = $uploadDbPath . $file['name'];
                }
            } else {
                unset($this->request->data['Game']['go_img']);
            }

            if ($this->Game->save($this->request->data)) {
                $this->Session->setFlash(__('The game has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The game could not be saved. Please, try again.'));
            }
        }
        $gameLengths = $this->Game->GameLength->find('list');
        $groupSizes = $this->Game->GroupSize->find('list');
        $ageGroups = $this->Game->AgeGroup->find('list');
        $categories = $this->Game->Category->find('list');
        $levels = $this->Game->Level->find('list');
        $this->set(compact('gameLengths', 'groupSizes', 'ageGroups', 'categories', 'levels'));

	}


/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Game->exists($id)) {
			throw new NotFoundException(__('Invalid game'));
		}
		if ($this->request->is(array('post', 'put'))) {

             $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
             $uploadPath = WWW_ROOT . 'img/uploads/';
             $uploadDbPath = '/img/uploads/';

             if(!empty($this->data['Game']['overview_img']['name'])) {
                 $file = $this->data['Game']['overview_img'];
                 $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                 if(in_array($ext, $arr_ext)) {
                     move_uploaded_file($file['tmp_name'], $uploadPath . $file['name']);
                     $this->request->data['Game']['overview_img'] = $uploadDbPath . $file['name'];
                 }
             } else {
                 unset($this->request->data['Game']['overview_img']);
             }

             if(!empty($this->data['Game']['ready_img']['name'])) {
                 $file = $this->data['Game']['ready_img'];
                 $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                 if(in_array($ext, $arr_ext)) {
                     move_uploaded_file($file['tmp_name'], $uploadPath . $file['name']);
                     $this->request->data['Game']['ready_img'] = $uploadDbPath . $file['name'];
                 }
             } else {
                 unset($this->request->data['Game']['ready_img']);
             }

             if(!empty($this->data['Game']['set_img']['name'])) {
                 $file = $this->data['Game']['set_img'];
                 $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                 if(in_array($ext, $arr_ext)) {
                     move_uploaded_file($file['tmp_name'], $uploadPath . $file['name']);
                     $this->request->data['Game']['set_img'] = $uploadDbPath . $file['name'];
                 }
             } else {
                 unset($this->request->data['Game']['set_img']);
             }

             if(!empty($this->data['Game']['go_img']['name'])) {
                 $file = $this->data['Game']['go_img'];
                 $ext = substr(strtolower(strrchr($file['name'], '.')), 1);
                 if(in_array($ext, $arr_ext)) {
                     move_uploaded_file($file['tmp_name'], $uploadPath . $file['name']);
                     $this->request->data['Game']['go_img'] = $uploadDbPath . $file['name'];
                 }
             } else {
                 unset($this->request->data['Game']['go_img']);
             }

			if ($this->Game->save($this->request->data)) {
				$this->Session->setFlash(__('The game has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The game could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Game.' . $this->Game->primaryKey => $id));
			$this->request->data = $this->Game->find('first', $options);
		}
		$gameLengths = $this->Game->GameLength->find('list');
		$groupSizes = $this->Game->GroupSize->find('list');
		$ageGroups = $this->Game->AgeGroup->find('list');
		$categories = $this->Game->Category->find('list');
		$levels = $this->Game->Level->find('list');
		$this->set(compact('gameLengths', 'groupSizes', 'ageGroups', 'categories', 'levels'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Game->id = $id;
		if (!$this->Game->exists()) {
			throw new NotFoundException(__('Invalid game'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Game->delete()) {
			$this->Session->setFlash(__('The game has been deleted.'));
		} else {
			$this->Session->setFlash(__('The game could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
