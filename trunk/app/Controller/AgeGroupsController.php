<?php
App::uses('AppController', 'Controller');
/**
 * AgeGroups Controller
 *
 * @property AgeGroup $AgeGroup
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AgeGroupsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->AgeGroup->recursive = 0;
		$this->set('ageGroups', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->AgeGroup->exists($id)) {
			throw new NotFoundException(__('Invalid age group'));
		}
		$options = array('conditions' => array('AgeGroup.' . $this->AgeGroup->primaryKey => $id));
		$this->set('ageGroup', $this->AgeGroup->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->AgeGroup->create();
			if ($this->AgeGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The age group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The age group could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->AgeGroup->exists($id)) {
			throw new NotFoundException(__('Invalid age group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AgeGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The age group has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The age group could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AgeGroup.' . $this->AgeGroup->primaryKey => $id));
			$this->request->data = $this->AgeGroup->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->AgeGroup->id = $id;
		if (!$this->AgeGroup->exists()) {
			throw new NotFoundException(__('Invalid age group'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->AgeGroup->delete()) {
			$this->Session->setFlash(__('The age group has been deleted.'));
		} else {
			$this->Session->setFlash(__('The age group could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
