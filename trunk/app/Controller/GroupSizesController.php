<?php
App::uses('AppController', 'Controller');
/**
 * GroupSizes Controller
 *
 * @property GroupSize $GroupSize
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class GroupSizesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->GroupSize->recursive = 0;
		$this->set('groupSizes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->GroupSize->exists($id)) {
			throw new NotFoundException(__('Invalid group size'));
		}
		$options = array('conditions' => array('GroupSize.' . $this->GroupSize->primaryKey => $id));
		$this->set('groupSize', $this->GroupSize->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->GroupSize->create();
			if ($this->GroupSize->save($this->request->data)) {
				$this->Session->setFlash(__('The group size has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The group size could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->GroupSize->exists($id)) {
			throw new NotFoundException(__('Invalid group size'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->GroupSize->save($this->request->data)) {
				$this->Session->setFlash(__('The group size has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The group size could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('GroupSize.' . $this->GroupSize->primaryKey => $id));
			$this->request->data = $this->GroupSize->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->GroupSize->id = $id;
		if (!$this->GroupSize->exists()) {
			throw new NotFoundException(__('Invalid group size'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->GroupSize->delete()) {
			$this->Session->setFlash(__('The group size has been deleted.'));
		} else {
			$this->Session->setFlash(__('The group size could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
