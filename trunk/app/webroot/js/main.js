var ctxApp = ctxApp || {};

(function(context) {
    context.handleBtnToggleClick = function(e) {
        $('#filters').slideToggle();
    };

    context.handleBtnSearchClick = function(e) {
        e.preventDefault();
        context.search();
    };

    context.search = function() {
        $('.top-rated-container').slideUp();
        $('#filters').slideUp();
        $('.search-results-container').slideUp();
        $('.search-results-loader').slideDown();

        $.ajax({
            url: '/api/searchGames',
            method: 'post',
            dataType: 'json',
            data: $('#filters').find('form').serialize(),
            success: function(response) {
                $('#searchResults').html('');
                for (var i in response) {
                    var game = response[i];
                    var $html = $('#searchResultTemplate').clone();
                    $html.removeClass('hide');
                    $html.find('.link').attr('href', '/games/view/' + game.Game.id);
                    $html.find('.name').html(game.Game.name);
                    $html.find('.groupSize').html(game.Game.group_size_name);
                    $html.find('.ageGroup').html(game.Game.age_group_name);
                    $html.find('.highFives').html(game.Game.num_highfives);
                    $html.find('.gameLength').html(game.Game.game_length_name);
                    if (game.Game.overview_img != '') {
                        $html.find('.thumbGame').attr('src',game.Game.overview_img);
                    }
                    $('#searchResults').append($html);
                }
                if (response.length == 0) {
                    $('#searchResults').append('<p class="text-center">No results for the selected criteria.</p>');
                }
                $('.search-results-loader').slideUp();
                $('.search-results-container').slideDown();
            }
        });
    };
})(ctxApp);

var ctxGame = ctxGame || {};

(function(context) {
    context.hasVoted = false;

    context.handleBtnHighFiveClick = function(e) {
        e.preventDefault();
        var self = this;
        if (context.hasVoted) return;

        $(this).find('img').fadeOut(function() {
            $(self).append('<h2>Thanks!</h2>');
            $(self).find('h2').hide().fadeIn();
        });

       context.hasVoted = true;

        $.ajax({
            url: '/api/highFive/' + $(this).data('gameid'),
            dataType: 'json',
            method: 'get',
            success: function(response) {
                if (response.error) {
                    alert('Error Voting: ' + response.error);
                }
            }
        });
    }
})(ctxGame);

$(function () {
    $('.btn-toggle-filter').click(ctxApp.handleBtnToggleClick);
    $('.btn-search').click(ctxApp.handleBtnSearchClick);
    $('.btn-high-five').click(ctxGame.handleBtnHighFiveClick);
});